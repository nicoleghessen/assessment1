HOSTNAME=$1
KEY=~/.ssh/nicole-key-aws.pem

# Log in to remote machine
ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$HOSTNAME '

sudo apt install haproxy

sudo sh -c/etc/haproxy/haproxy.cfg


frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server <ubuntu> <172.31.14.94>:80 check
   server <awslinux> <172.31.37.119>:80 check
   '