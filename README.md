### Assessment 1

- start the 3 machines
- tag them appropriately 
- Then focus on HA proxy
- The task is to create 2 web servers that are served from a single public IP address behind a Load balancer called haproxy. 



### Create 3 instances on AWS
Make 1 ubuntu machines, 1 AWS Linux and 1 Load balancer on Ubuntu
Include tags
Security Group: with port 80 open to world and port 22 open to you.

### Setup Nginx on 1 of the machines 
using the script named: "ubuntu_nginx.sh" you can download and start nginx on your first machine. 


### Next setup Apache on 1 other machine
follow the script named "awslinux_apache.sh" which will successfully download and install apache on your server and display the hompage which has been edited- edit the contents of your page within the code before deploying it.



### Load balancer
- The load balancer should be set up to see your 2 web servers private IP addresses and balance in a round robin way.  
- This will need to be the last machine to start as you need to get the private IP addresses from your other 2 servers.
- You should see each servers page on a refresh in turn.  

### Now configure the HAproxy on the last VM: this can be done manually but ideally you can follow script "haproxy_lb.sh" to also do this for you.

Logged into your ubuntu machine- to install HAproxy type the command:
```bash 

$ sudo apt install haproxy

```

### Then edit in nano mode to add your server name and the private IP to connect them
```bash
$ sudo nano /etc/haproxy/haproxy.cfg


frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server <server1 name> <private IP 1>:80 check
   server <server2 name> <private IP 2>:80 check
   ```

### Refresh the third machine page and your new welcome page which you edited should appear.
 
 Then restart it and check the status: 

```bash
$ sudo systemctl restart haproxy
$ sudo systemctl status haproxy
```

## Having problems with installing haproxy even manually.





Public IP: 

Ubuntu: 54.171.109.191
 
AWS IP: 63.35.251.122

LB: 54.229.160.223

